# laiwan-game

## 概述：
    工作之余练手项目。可用于局制游戏服务端，其他游戏类型也可以支持，实现了集群部署应付压力。
> [网关服务节点](http://gitee.com/noah1100/laiwan-game/tree/master/service-gateway-websocket?_blank)：接受客户端请求，分发到各处理节点（大厅、房间、游戏）。    

> [大厅服务节点](http://gitee.com/noah1100/laiwan-game/tree/master/service-logic-hall?_blank)：用户登录、注册、充值等等。

> [房间服务节点](http://gitee.com/noah1100/laiwan-game/tree/master/service-room?_blank):用户创建房间，解散房间，房内聊天等等。

> [游戏服务节点](http://gitee.com/noah1100/laiwan-game/tree/master/service-game?_blank):游戏节点， 五子棋啊，斗地主啊，麻将啊，狼人杀啊等等 

> [游戏示例-《猜拳游戏》](http://gitee.com/noah1100/laiwan-game/tree/master/game-caiquan)

> [定时任务处理节点](http://gitee.com/noah1100/laiwan-game/tree/master/service-quartz?_blank): 定时任务通过回调消息来执行

## eclipse运行方式,启动顺序随意：
>大厅节点：HallVerticle.java

>网关节点：GatewayVerticle.java

>房间节点：RoomVerticle.java

>定时任务节点：QuartzVerticle.java

>游戏节点：启动猜拳游戏下的启动类即可：StartMain.java

>多游戏部署的话，把游戏的jar放到game节点下的classpath，启动GameVerticle.java类即可。
    
## 使用技术：
>集群方案：vertx3+hazelcast

>缓存方案：redis没的说

>数据库：mysql,如果数据量上百千万的时候数据库瓶颈的话 [我想说TIDB了解一下？](https://pingcap.com?_blank)

## 流程图：
![输入图片说明](https://gitee.com/uploads/images/2018/0416/171339_dcb18b6d_485050.png "未命名文件.png")

## 架构图：
![输入图片说明](https://gitee.com/uploads/images/2018/0412/213510_3b343630_485050.png "小游戏服务器 (1).png")

## 待完成：
1.数据库缓存数据到redis
2.redis集群