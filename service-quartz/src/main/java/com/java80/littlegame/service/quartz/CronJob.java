package com.java80.littlegame.service.quartz;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class CronJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		Object usertask = jobDataMap.get(CronTask.TASK_KEY);
		Object l = jobDataMap.get(CronTask.TASK_KEY_CRON);
		if (l != null && l instanceof CronTask) {
			System.out.println("crontask callback");
			((CronTask) l).end();
		}
		if (usertask != null && usertask instanceof CronTask) {
			System.out.println("crontask callback");
			((CronTask) usertask).end();
		}

	}

}
