package com.java80.littlegame.service.quartz;

import java.util.Properties;

import com.java80.littlegame.common.utils.LoadPropertiesFileUtil;

public class QuartzConfig {
	private static String queueName;
	private static String serviceId;
	private static String instanceName;
	static {
		init();
	}

	public static void init() {
		Properties p = LoadPropertiesFileUtil.loadProperties("../config/cfg.properties");
		queueName = p.getProperty("service.quartz.self.queuename");
		serviceId = p.getProperty("service.quartz.id");
		instanceName = p.getProperty("service.quartz.insname");

	}

	public static String getQueueName() {
		return queueName;
	}

	public static String getServiceId() {
		return serviceId;
	}

	public static String getInstanceName() {
		return instanceName;
	}
}
