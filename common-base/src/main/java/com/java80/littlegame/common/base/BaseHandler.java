package com.java80.littlegame.common.base;

public abstract class BaseHandler {
	/**
	 * obj 是json
	 * 
	 * @param obj
	 * @return
	 */

	/*
	 * public void refreshService(String serviceId, String queueName, int
	 * serviceType) { ServiceStatus ss = new ServiceStatus();
	 * ss.setLastTime(TimeUtil.getCurrentTime()); ss.setServiceId(serviceId);
	 * ss.setServiceQueueName(queueName); ss.setServiceType(serviceType);
	 * CacheService.makeObjMap(ss.getDBIdx(), SystemConsts.SERVICE_STATUS_KEY,
	 * ss); }
	 */
	public abstract Object onReceive(String obj);

	public static void addTask(BaseHandler h, String obj) {
		ThreadPools.addTask(new Task() {
			@Override
			public void work() {
				h.onReceive(obj);
			}
		});
	}
}
