package com.java80.littlegame.common.message.proto;

public class ProtoList {
	public static final int MSG_TYPE_HALL = 1;
	public static final int MSG_TYPE_ROOM = 2;
	public static final int MSG_TYPE_GAME = 3;
	public static final int MSG_TYPE_TIMER = 8;
	public static final int MSG_TYPE_CLUSTER = 9;

	public static final int MSG_CODE_LOGIN = 10001;
	public static final int MSG_CODE_REGISTER = 10002;
	public static final int MSG_CODE_PULLANDPUSHGAMESYSTEM = 10003;

	public static final int MSG_CODE_CREATE_ROOM = 20001;// 开房
	public static final int MSG_CODE_JION_ROOM = 20002;// 加入
	public static final int MSG_CODE_UPDATESTATUS_ROOM = 20003;// 更改房间状态,锁定操作等等
	public static final int MSG_CODE_USERJOINROOM = 20004;// 通知其他人有人加入

	public static final int MSG_CODE_GAME_START = 90001;
	public static final int MSG_CODE_DESKEND = 90002;

	public static final int MSG_CODE_CAIQUANGAMESTART = 30001;
	public static final int MSG_CODE_CAIQUANACTION = 30002;
	public static final int MSG_CODE_CAIQUANACTION_RESULT = 30003;
	public static final int MSG_CODE_CAIQUANASETTLE = 30004;
	public static final int MSG_CODE_DESKTOPEND = 30005;

	public static final int MSG_CODE_TIMER_DESKTOPEXPRIDED = 80001;
}
