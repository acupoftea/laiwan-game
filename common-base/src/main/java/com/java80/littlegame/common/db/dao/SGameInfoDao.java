package com.java80.littlegame.common.db.dao;

import com.java80.littlegame.common.db.dao.base.BaseSysDaoInterface;
import com.java80.littlegame.common.db.entity.SGameInfo;

public interface SGameInfoDao extends BaseSysDaoInterface<SGameInfo> {
}
